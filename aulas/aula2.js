var http = require('http'); // módulo http

var server = http.createServer(function(req, res){ // request and response
    res.writeHead(200,{'Content-Type': "text/html"});

    if (req.url == '/') {
        res.write('<h1>Hello World!</h1>');
    }else if(req.url == '/pagina1'){
        res.write('<h1>Página 1</h1>');
    }else{
        res.write('<h1>Erro 404</h1>');
    }

    res.end();
});

server.listen(3000, function(){ //ligando o servidor, callback é opicional
    console.log('Servidor Online.');
});

// rodar: node aula1.js
