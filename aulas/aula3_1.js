var http    = require('http'); // módulo http
var fs      = require('fs');

var server = http.createServer(function(req, res){ // request and response
    res.writeHead(200,{'Content-Type': "text/html"});
    res.write('<h1>Hello World!</h1>');
    var log = fs.createWriteStream('./arquivos/logs.txt', {flags: 'a'}); // a = não sobrescrever o arquivo
    log.write(req.url+'\n');
    res.end();
});

server.listen(3000, function(){ //ligando o servidor, callback é opicional
    console.log('Servidor Online.');
});

// rodar: node aula1.js
