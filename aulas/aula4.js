var http    = require('http');
var fs      = require('fs');

var server = http.createServer(function(req, res){

    if(req.url == '/contatos'){
        fs.readFile(__dirname + '/contato.html', function(err, data){
            if(err) throw err;
            res.writeHead(200,{'Content-Type': "text/html"});
            res.write(data);
            res.end();
        });
    }else{
        fs.readFile(__dirname + '/index.html', function(err, data){
            if(err) throw err;
            res.writeHead(200,{'Content-Type': "text/html"});
            res.write(data);
            res.end();
        });
    }

});

server.listen(3000, function(){
    console.log('Servidor Rodando')
});
