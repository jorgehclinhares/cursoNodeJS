// module.exports = function(nome){  // maneira de fazer quando existe apenas 1 export no módulo
//     console.log('o nome é ' + nome);
// }

exports.meuNome = function(nome){
    console.log('o nome é ' + nome);
}

exports.minhaIdade = function(idade){
    console.log('a idade é ' + idade);
}
