var express = require('express');
var router  = express.Router();

//modelo
var Usuario = require('../models/users.js');

router.get('/users', function(req, res, next) {

  Usuario.find(function(err,data){
      if(err){
          console.log(err);
          return;
      }
      res.render('users', { title: 'Express', lista: data });
  });

});

router.get('/users/create', function(req, res, next) { res.render('create'); });

router.post('/users/edit/:id', function(req, res, next) {

    Usuario.findById(req.params.id, function(err, data){
        if(err){
            console.log(err);
            return;
        }

        var model   = data;
        model.nome  = req.body.nome;
        model.login = req.body.login;
        model.senha = req.body.senha;

        model.save(function(err){
            if(err){
                console.log(err);
                return;
            }
            res.redirect('/users');
        });
    });
});

router.get('/users/edit/:id', function(req, res, next) {
    Usuario.findById(req.params.id, function(err, data){
        if(err){
            console.log(err);
            return;
        }
        res.render('edit', {value: data});
    });
});

router.post('/users/create', function(req, res, next) {

    var usuario = new Usuario(req.body);
    usuario.save(function(err){
        if(err){
            console.log(err);
            return;
        }
        res.redirect('/users');
    });

});

router.post('/users/delete/:id', function(req, res, next) {

    Usuario.remove({ _id: req.params.id }, function(err){
        if(err){
            console.log(err);
            return;
        }
        res.redirect('/users');
    });

});

module.exports = router;
