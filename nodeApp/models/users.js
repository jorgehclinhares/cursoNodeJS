var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var usuario     = new Schema({
    nome: String,
    login: String,
    senha: String,
    data:  { type: Date, default: Date.now }
});

module.exports = mongoose.model('Usuario', usuario);
